enum Value: Int {
	case two, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace
	static let allValues = [two, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace]
}
