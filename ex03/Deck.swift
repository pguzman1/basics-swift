import Foundation

class Deck: NSObject {
    static let allSpades: [Card] = Value.allValues.map({
        return Card(color: Color.spade, value: $0)
    })
    static let allDiamonds: [Card] = Value.allValues.map({
        return Card(color: Color.diamond, value: $0)
    })
    static let allHearts: [Card] = Value.allValues.map({
        return Card(color: Color.heart, value: $0)
    })
    static let allClubs: [Card] = Value.allValues.map({
        return Card(color: Color.club, value: $0)
    })

    static let allCards: [Card] = allSpades + allDiamonds + allHearts + allClubs

}

extension Array {
    mutating func shuffle() {
        let length = self.count
		var tmp: Element?
        for i in 0 ... length - 1 {
            let randomNumber = Int(arc4random_uniform(UInt32(self.count - 1)))
            if (i != randomNumber) {
//                swap(&self[i], &self[randomNumber])
                tmp = self[i]
				self[i] = self[randomNumber]
				self[randomNumber] = tmp!
            }
        }
    }
}
