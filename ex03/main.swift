var myCards = Deck.allCards
myCards.shuffle()
print("\n This is a shuffled deck of cards: \n")
for card in myCards {
    print(card)
}
