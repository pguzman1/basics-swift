print("Color.allColors: \n")
for color in Color.allColors {
    print(color)
}
print("\nValue.allValues: \n")
for value in Value.allValues {
    print(value)
}
print("\naccess to color and a value: \n")
print("A Heart : ", Color.heart)

print("A King : ", Value.king)
