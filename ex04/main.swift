var notShuffleDeck = Deck(shuffled: false)
print("\n a deck not shuffled\n")
for card in notShuffleDeck.cards! {
	print(card)
}

var myDeck = Deck(shuffled: true)
print("\n a deck of cards when shuffled true\n")
for card in myDeck.cards!   {
    print(card)
}
print("\nmyDeck has been shuffled\n")
print("\nfirst card of my deck is \(myDeck.cards![0])\n")
let draw = myDeck.draw()
print("\ni draw a card => \(draw!)\n")
print("\nfirst card of my desk now is \(myDeck.cards![0])\n")
print("\nmy Hand is (outs) => \(myDeck.outs)\n")
print("\nI fold a card that is not in my hand (outs)\n")
myDeck.fold(c: Card(color: Color.club, value: Value.eight))
print("\nnothing happens\n")
print("\ni fold a card that i previously draw\n")
myDeck.fold(c: draw!)
print("\nnow it is un discards\n")
print("\nmyDeck.discards => \(myDeck.discards)\n")
print("\nmyDeck.outs => \(myDeck.outs)\n")
print("\nmy Deck description => \(myDeck.description)\n")
