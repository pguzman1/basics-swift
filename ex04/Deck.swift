import Foundation

class Deck: NSObject {
    static let allSpades: [Card] = Value.allValues.map({
        return Card(color: Color.spade, value: $0)
    })
    static let allDiamonds: [Card] = Value.allValues.map({
        return Card(color: Color.diamond, value: $0)
    })
    static let allHearts: [Card] = Value.allValues.map({
        return Card(color: Color.heart, value: $0)
    })
    static let allClubs: [Card] = Value.allValues.map({
        return Card(color: Color.club, value: $0)
    })

    static let allCards: [Card] = allSpades + allDiamonds + allHearts + allClubs

    var cards: [Card]?
    var discards: [Card] = []
    var outs: [Card] = []

    init(shuffled: Bool) {
        cards = Deck.allCards
        if (shuffled) {
            cards?.shuffle()
        }
    }

    override var description: String {
        return cards!.description
    }

    func draw() -> Card? {
        if (cards!.count > 0) {
            let cardToDraw = cards![0]
            cards!.remove(at: 0)
            self.outs.append(cardToDraw)
            return cardToDraw
        }
        else {
            return nil
        }
    }

    func fold(c: Card) {
        if let index = outs.index(of: c) {
            let cardToFold = outs[index]
            discards.append(cardToFold)
            outs.remove(at: index)
        }
    }

}

extension Array {
    mutating func shuffle() {
        let length = self.count
		var tmp: Element?
        for i in 0 ... length - 1 {
            let randomNumber = Int(arc4random_uniform(UInt32(self.count - 1)))
            if (i != randomNumber) {
               // swap(&self[i], &self[randomNumber])
				tmp = self[i]
				self[i] = self[randomNumber]
				self[randomNumber] = tmp!
            }
        }
    }
}
