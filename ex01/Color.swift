enum Color: String {
	case diamond, heart, spade, club
	static let allColors = [diamond, heart, spade, club]
}
