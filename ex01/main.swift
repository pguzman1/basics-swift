let newCard = Card(color: Color.spade, value: Value.two)
print("newCard => \(newCard)\n")
let anotherCard = Card(color: Color.heart, value: Value.two)
print("anotherCard => \(anotherCard)\n")
let againAnotherCard = Card(color: Color.heart, value: Value.two)
print("againAnotherCard => \(againAnotherCard)\n")
//                                DIFFERENT CARDS
print("newCard == anotherCard => \(newCard == anotherCard)\n") // false
//                                SAME CARDS
print("anotherCard == againAnotherCard => \(anotherCard == againAnotherCard)\n") //true
//                                DESCRIPTION OF A CARD
print("newCard.description => \(newCard.description)\n")
